{if $staff_data}
    {assign var="id" value=$staff_data.staff_id}
{else}
    {assign var="id" value=""}
{/if}

{capture name="mainbox"}

<form action="{""|fn_url}" method="post" class="form-horizontal form-edit" name="staff_form" enctype="multipart/form-data">
<input type="hidden" class="cm-no-hide-input" name="fake" value="1" />
<input type="hidden" class="cm-no-hide-input" name="staff_id" value="{$id}" />

{capture name="tabsbox"}
<div id="content_general">
    <div class="control-group">
        <label for="elm_staff_f_name" class="control-label">{__("first_name")}</label>
        <div class="controls">
        <input type="text" name="staff_data[f_name]" id="elm_staff_f_name" value="{$staff_data.f_name}" size="25" class="input-large" /></div>
    </div>
	
	<div class="control-group">
        <label for="elm_staff_l_name" class="control-label">{__("last_name")}</label>
        <div class="controls">
        <input type="text" name="staff_data[l_name]" id="elm_staff_l_name" value="{$staff_data.l_name}" size="25" class="input-large" /></div>
    </div>
	
	<div class="control-group">
        <label for="elm_staff_email" class="control-label">{__("email")}</label>
        <div class="controls">
        <input type="text" name="staff_data[email]" id="elm_staff_email" value="{$staff_data.email}" size="25" class="input-large" /></div>
    </div>
	
    <div class="control-group">
        <label class="control-label" for="elm_staff_function">{__("text_functions")}:</label>
        <div class="controls">
            <textarea id="elm_staff_function" name="staff_data[function]" cols="35" rows="8" class="cm-wysiwyg input-large">{$staff_data.function}</textarea>
        </div>
    </div>
    
    <div class="control-group">
        <label class="control-label" for="elm_staff_function">{__("user")}:</label>
        <div class="controls">
            {include file="addons/staff/pickers/users/picker.tpl" data_id="return_users" display="radio" item_ids=$staff_data.user input_name="staff_data[user]" type="table" picker_for=""}
        </div>
    </div>
    
	<div class="control-group" id="staff_graphic">
        <label class="control-label">{__("image")}</label>
        <div class="controls">
            {include file="common/attach_images.tpl" image_name="staff" image_object_type="staff" image_pair=$staff_data.main_pair image_object_id=$id no_detailed=true hide_titles=true}
        </div>
    </div>
</div>
{/capture}
{include file="common/tabsbox.tpl" content=$smarty.capture.tabsbox active_tab=$smarty.request.selected_section track=true}

{capture name="buttons"}
    {if !$id}
        {include file="buttons/save_cancel.tpl" but_role="submit-link" but_target_form="staff_form" but_name="dispatch[staff.update]"}
    {else}
        {include file="buttons/save_cancel.tpl" but_name="dispatch[staff.update]" but_role="submit-link" but_target_form="staff_form" save=$id}
    {/if}
{/capture}
    
</form>

{/capture}

{if !$id}
    {assign var="title" value=__("new_staff")}
{else}
    {assign var="title" value="{__("editing_staff")}: {$staff_data.f_name} {$staff_data.l_name}"}
{/if}
{include file="common/mainbox.tpl" title=$title content=$smarty.capture.mainbox buttons=$smarty.capture.buttons select_languages=true}

{** staff section **}
