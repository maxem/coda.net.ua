{if !$smarty.request.extra}
<script type="text/javascript">
(function(_, $) {
    _.tr('text_items_added', '{__("text_items_added")|escape:"javascript"}');

    $.ceEvent('on', 'ce.formpost_add_users_form', function(frm, elm) {
        var users = {};

        if ($('input.cm-item:checked', frm).length > 0) {

            $('input.cm-item:checked', frm).each( function() {
                var id = $(this).val();
                var item = $(this).parent().siblings();
                users[id] = {
                    email: item.find('.user-email').text(), 
                    user_name: item.find('.user-name').text()
                };
            });

            {literal}
            $.cePicker('add_js_item', frm.data('caResultId'), users, 'u', {
                '{user_id}': '%id',
                '{email}': '%item.email',
                '{user_name}': '%item.user_name'
            });
            {/literal}
            
            $.ceNotification('show', {
                type: 'N', 
                title: _.tr('notice'), 
                message: _.tr('text_items_added'), 
                message_state: 'I'
            });
        }

        return false;        
    });
}(Tygh, Tygh.$));
</script>
{/if}


<form action="{$smarty.request.extra|fn_url}" method="post" data-ca-result-id="{$smarty.request.data_id}" name="add_users_form">

{include file="common/pagination.tpl" save_current_page=true div_id="pagination_`$smarty.request.data_id`"}

{if $staff}
<table width="100%" class="table table-middle">
<thead>
<tr>
    <th width="1%" class="center">
        {if $smarty.request.display == "checkbox"}
        {include file="common/check_items.tpl"}</th>
        {/if}
    <th>{__("id")}</th>
    <th>{__("email")}</th>
    <th>{__("person_name")}</th>
    <th>{__("text_functions")}</th>
</tr>
</thead>
{foreach from=$staff item=user}
<tr>
    <td class="left">
        <input type="checkbox" name="add_users[]" value="{$user.staff_id}" class="cm-item" />
    </td>
    <td>{$user.staff_id}</td>
    <td><span class="user-name">{if $user.f_name || $user.l_name}{$user.f_name} {$user.l_name}{else}-{/if}</span></td>
    <td><span class="user-email">{$user.email}</span></td>
    <td>{$user.function}</td>
</tr>
{/foreach}
</table>
{else}
    <p class="no-items">{__("no_data")}</p>
{/if}

{include file="common/pagination.tpl" div_id="pagination_`$smarty.request.data_id`"}

<div class="buttons-container">
    {assign var="but_close_text" value=__("choose")}
    {include file="buttons/add_close.tpl" is_js=$smarty.request.extra|fn_is_empty}
</div>

</form>
