<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;
use Tygh\Settings;
use Tygh\RestClient;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

/**
 * Handlers
 */

function fn_yandex_metrika_oauth_info()
{
    if (
        !fn_string_not_empty(Registry::get('addons.rus_yandex_metrika.application_id'))
        || !fn_string_not_empty(Registry::get('addons.rus_yandex_metrika.application_password'))
    ) {
        return __('yandex_metrika_oauth_info_part1', array(
            '[callback_uri]' => fn_url('yandex_metrika_tools.oauth')
        ));
    } else {
        $client_id = Registry::get('addons.rus_yandex_metrika.application_id');

        return __('yandex_metrika_oauth_info_part2', array(
            '[auth_uri]' => "https://oauth.yandex.ru/authorize?response_type=code&client_id=" . $client_id,
            '[edit_app_uri]' => "https://oauth.yandex.ru/client/edit/" . $client_id,
        ));
    }
}

/**
 * \Handlers
 */

/**
 * Common functions
 */

function fn_yandex_metrika_sync_goals()
{
    $oauth_token = Settings::instance()->getValue('auth_token', 'rus_yandex_metrika');
    $counter_number = Settings::instance()->getValue('counter_number', 'rus_yandex_metrika');

    if (empty($oauth_token) || empty($counter_number)) {
        return false;
    }

    $goals_scheme = fn_get_schema('rus_yandex_metrika', 'goals');
    $selected_goals = Settings::instance()->getValue('collect_stats_for_goals', 'rus_yandex_metrika');

    $ext_goals = array();
    $res = fn_yandex_metrika_rest_client('get', "/counter/$counter_number/goals.json", array('oauth_token' => $oauth_token));

    if (!empty($res['goals'])) {
        foreach ($res['goals'] as $goal) {
            $ext_goals[$goal['name']] = $goal;
        }

    } elseif ($res === false) {
        return false;
    }

    foreach ($goals_scheme as $goal_name => $goal) {
        $ext_goal_name = '[auto] ' . $goal['name'];
        if (!empty($ext_goals[$ext_goal_name])) {
            if (empty($selected_goals[$goal_name]) || $selected_goals[$goal_name] == 'N') {
                fn_yandex_metrika_rest_client('delete', "/counter/$counter_number/goal/" . $ext_goals[$ext_goal_name]['id'] . "?oauth_token=$oauth_token");
            }
        } else {
            if (!empty($selected_goals[$goal_name]) && $selected_goals[$goal_name] == 'Y') {
                $goal['name'] = $ext_goal_name;
                fn_yandex_metrika_rest_client('post', "/counter/$counter_number/goals?oauth_token=$oauth_token", array('goal' => $goal));
            }
        }
    }

    return true;
}

function fn_yandex_metrika_rest_client($type, $url, $data = array())
{
    static $client = null;

    if (!isset($client)) {
        $client = new RestClient('https://api-metrika.yandex.ru/');
    }

    $res = false;

    try {
        if ($type == 'get') {
            $res = $client->get($url, $data);

        } elseif ($type == 'post') {
            $client->post($url, $data);

        } elseif ($type == 'delete') {
            $client->delete($url);
        }
    } catch (\Pest_Unauthorized $e) {
        fn_set_notification('E', __('error'), '401 Unauthorized. '. __('yandex_metrika_pest_unauthorized'));

    }  catch (\Pest_NotFound $e) {
        fn_set_notification('E', __('error'), '404 Not Found. ' . __('yandex_metrika_pest_not_found'));

    }  catch (\Pest_Forbidden $e) {
        fn_set_notification('E', __('error'), '403 Forbidden. ' . __('yandex_metrika_pest_forbidden'));

    } catch (\Exception $e){
        fn_set_notification('E', __('error'), strip_tags($e->getMessage()));
    }

    return $res;
}

/**
 * \Common functions
 */
