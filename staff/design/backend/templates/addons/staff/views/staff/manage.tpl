{** staff section **}

{capture name="mainbox"}

<form action="{""|fn_url}" method="post" name="staff_form" enctype="multipart/form-data">
<input type="hidden" name="fake" value="1" />

{if $staff}
<table class="table table-middle">
<thead>
<tr>
    <th width="1%" class="left">
        {include file="common/check_items.tpl" class="cm-no-hide-input"}</th>
    <th>{__("name")}</th>
    <th>{__("text_functions")}</th>
    <th>{__("email")}</th>
    <th>{__("user")}</th>
    <th></th>
</tr>
</thead>
{foreach from=$staff item=user}
<tr>
    <td class="left"><input type="checkbox" name="user_ids[]" value="{$user.staff_id}" class="cm-item" /></td>
    <td class="{$no_hide_input}"><a href="{"staff.update?staff_id=`$user.staff_id`"|fn_url}">{$user.f_name} {$user.l_name}</a></td>
    <td class="nowrap row-status">{$user.function}</td>
    <td class="nowrap row-status">{$user.email}</td>
    <td>{if $user.user > 0}<a class="row-status" href="{"profiles.update?user_id=`$user.user`"|fn_url}">User</a>{/if}</td>
    <td>
        {capture name="tools_list"}
            <li>{btn type="list" text=__("edit") href="staff.update?staff_id=`$user.staff_id`"}</li>
            <li>{btn type="list" class="cm-confirm cm-post" text=__("delete") href="staff.delete?staff_id=`$user.staff_id`"}</li>
        {/capture}
        <div class="hidden-tools">
            {dropdown content=$smarty.capture.tools_list}
        </div>
    </td>
</tr>
{/foreach}
</table>
{else}
    <p class="no-items">{__("no_data")}</p>
{/if}

{capture name="buttons"}
    {capture name="tools_list"}
        {if $staff}
            <li>{btn type="delete_selected" dispatch="dispatch[staff.m_delete]" form="staff_form"}</li>
        {/if}
    {/capture}
    {dropdown content=$smarty.capture.tools_list}
{/capture}
{capture name="adv_buttons"}
    {include file="common/tools.tpl" tool_href="staff.add" prefix="top" hide_tools="true" title=__("add_staff") icon="icon-plus"}
{/capture}

</form>

{/capture}
{include file="common/mainbox.tpl" title=__("staff") content=$smarty.capture.mainbox buttons=$smarty.capture.buttons adv_buttons=$smarty.capture.adv_buttons select_languages=true}

{** ad section **}