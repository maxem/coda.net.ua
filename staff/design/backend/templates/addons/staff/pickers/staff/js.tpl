<tr {if !$clone}id="{$holder}_{$user_id}" {/if}class="cm-js-item{if $clone} cm-clone hidden{/if}">
    <td><input type="text" name="{$input_name}[{$user_id}]" value="{math equation="a*b" a=$position b=10}" size="3" class="input-micro" {if $clone}disabled="disabled"{/if} /></td>
    <td>{$user_name} (<a href="{"staff.update&staff_id=`$user_id`"|fn_url}" class="user-email"><span>{$email}</span></a>)</td>
    <td class="nowrap">
        {if !$view_only}
            {capture name="tools_list"}
                <li>{btn type="list" text=__("edit") href="staff.update&staff_id=`$user_id`"}</li>
                <li>{btn type="list" text=__("remove") onclick="Tygh.$.cePicker('delete_js_item', '{$holder}', '{$user_id}', 'u'); return false;"}</li>
            {/capture}
            <div class="hidden-tools">
                {dropdown content=$smarty.capture.tools_list}
            </div>
        {/if}
    </td>
</tr>