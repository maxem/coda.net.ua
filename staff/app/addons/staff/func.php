<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;
use Tygh\Languages\Languages;
use Tygh\BlockManager\Block;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

function fn_get_staff_data($staff_id = false) {
	if (empty($staff_id)) {
        return false;
    }
    
    $staff_data = db_get_row("SELECT * FROM ?:staff WHERE ?:staff.staff_id = ?i", $staff_id);
    if (!empty($staff_data)) {
        $staff_data['main_pair'] = fn_get_image_pairs($staff_id, 'staff', 'M', true, false);
    }
    if (AREA == 'C') {
	        if (!empty($staff_data['user'])) {
		        $user_data = fn_get_user_info($staff_data['user'], true);
		        if (empty($staff_data['f_name'])) {
			        $staff_data['f_name'] = $user_data['firstname'];
		        }
		        if (empty($staff_data['l_name'])) {
			        $staff_data['l_name'] = $user_data['lastname'];
		        }
		        if (empty($staff_data['email'])) {
			        $staff_data['email'] = $user_data['email'];
		        }
	        }
	}
    return $staff_data;
}
function fn_update_staff($data, $staff_id, $lang_code = DESCR_SL)
{
    if (!empty($staff_id)) {
        db_query("UPDATE ?:staff SET ?u WHERE staff_id = ?i", $data, $staff_id);
    } else {
        $staff_id = $data['staff_id'] = db_query("INSERT INTO ?:staff ?e", $data);
    }
	fn_attach_image_pairs('staff', 'staff', $staff_id, $lang_code);
    return $staff_id;
}
function fn_get_staff($params = array(), $lang_code = CART_LANGUAGE)
{
    $default_params = array(
        'items_per_page' => 0,
    );


    $sortings = array(
        'email' => '?:staff.email',
        'name' => '?:staff.f_name',
    );

    $condition = $limit = '';

    if (!empty($params['limit'])) {
        $limit = db_quote(' LIMIT 0, ?i', $params['limit']);
    }

    $sorting = db_sort($params, $sortings, 'name', 'asc');
	
	if (!empty($params['item_ids'])) {
        $condition .= db_quote(' AND ?:staff.staff_id IN (?n)', explode(',', $params['item_ids']));
        $sorting = 'ORDER BY FIELD(?:staff.staff_id , '.$params['item_ids'].')';
    }
	
	$fields = array (
        '?:staff.staff_id',
        '?:staff.f_name',
        '?:staff.l_name',
        '?:staff.email',
        '?:staff.function',
        '?:staff.user',
    );

	$staff = db_get_hash_array(
        "SELECT ?p FROM ?:staff " .
        "WHERE 1 ?p ?p ?p",
        'staff_id', implode(", ", $fields), $condition, $sorting, $limit
    );

    foreach ($staff as $staff_id => $staff_item) {
        $staff[$staff_id]['main_pair'] = fn_get_image_pairs($staff_id, 'staff', 'M', true, false);
    }
    
	if (AREA == 'C') {
		foreach ($staff as $staff_id => $staff_item) {
	        if (!empty($staff_item['user'])) {
		        $user_data = fn_get_user_info($staff_item['user'], true);
		        if (empty($staff_item['f_name'])) {
			        $staff[$staff_id]['f_name'] = $user_data['firstname'];
		        }
		        if (empty($staff_item['l_name'])) {
			        $staff[$staff_id]['l_name'] = $user_data['lastname'];
		        }
		        if (empty($staff_item['email'])) {
			        $staff[$staff_id]['email'] = $user_data['email'];
		        }
	        }
	    }
	}

    return array($staff, $params);
}


function fn_delete_staff_by_id($staff_id)
{
    if (!empty($staff_id)) {
        db_query("DELETE FROM ?:staff WHERE staff_id = ?i", $staff_id);

        Block::instance()->removeDynamicObjectData('staff', $staff_id);
		fn_delete_image_pairs($staff_id, 'staff');
    }
}