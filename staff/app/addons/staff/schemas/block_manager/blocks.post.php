<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

$schema['staff'] = array (

    'content' => array(
        'items' => array(
            'type' => 'enum',
            'object' => 'staff',
            'items_function' => 'fn_get_staff',
            'remove_indent' => true,
            'hide_label' => true,
            'fillings' => array(
                'manually' => array(
                    'picker' => 'addons/staff/pickers/staff/picker.tpl',
                    'picker_params' => array(
                        'type' => 'links',
                        'positions' => true,
                    ),
                ),
            ),
        ),
    ),
    'templates' => array(
        'addons/staff/blocks/our_staff.tpl' => array(),
    ),
    'wrappers' => 'blocks/wrappers',
    'cache' => array(
        'update_handlers' => array(
            'staff',
            'images_links',
        ),
        'disable_cache_when' => array(
            'callable_handlers' => array(
                array('fn_block_products_disable_cache', array('$block_data'))
            ),
        )
    )
);

return $schema;
