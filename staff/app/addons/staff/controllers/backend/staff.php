<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;
use Tygh\Session;
use Tygh\Mailer;
use Tygh\Api;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if ($mode == 'm_delete') {

        if (!empty($_REQUEST['user_ids'])) {
            foreach ($_REQUEST['user_ids'] as $v) {
                fn_delete_staff_by_id($v);
            }
        }

        return array(CONTROLLER_STATUS_OK, 'staff.manage');
    }

    if ($mode == 'update' || $mode == 'add') {
        $staff_id = fn_update_staff($_REQUEST['staff_data'], $_REQUEST['staff_id'], DESCR_SL);
        return array(CONTROLLER_STATUS_OK, 'staff' . (!empty($staff_id) ? '.update&staff_id='.$staff_id.'' : '.add'));
    }

    if ($mode == 'delete') {
        fn_delete_staff_by_id($_REQUEST['staff_id']);

        return array(CONTROLLER_STATUS_REDIRECT, 'staff.manage');

    }
}

if ($mode == 'manage') {

    list($staff, ) = fn_get_staff(array(), DESCR_SL);

    Tygh::$app['view']->assign('staff', $staff);

} elseif ($mode == 'picker') {
    $params = $_REQUEST;
    $params['skip_view'] = 'Y';

    list($users, $search) = fn_get_users($params, $auth, Registry::get('settings.Appearance.admin_elements_per_page'));
    Tygh::$app['view']->assign('users', $users);
    Tygh::$app['view']->assign('search', $search);

    Tygh::$app['view']->assign('countries', fn_get_simple_countries(true, CART_LANGUAGE));
    Tygh::$app['view']->assign('states', fn_get_all_states());
    Tygh::$app['view']->assign('usergroups', fn_get_usergroups(array('status' => array('A', 'H')), CART_LANGUAGE));

    Tygh::$app['view']->display('addons/staff/pickers/users/picker_contents.tpl');
    exit;

} elseif ($mode == 'picker_staff') {
    $params = $_REQUEST;
    $params['skip_view'] = 'Y';

    list($staff, $search) = fn_get_staff($params, $auth, Registry::get('settings.Appearance.admin_elements_per_page'));
    Tygh::$app['view']->assign('staff', $staff);
    Tygh::$app['view']->assign('search', $search);

    Tygh::$app['view']->display('addons/staff/pickers/staff/picker_contents.tpl');
    exit;

} elseif ($mode == 'update' || $mode == 'add') {
	
	if ($mode == 'update') {
		$staff_data = fn_get_staff_data($_REQUEST['staff_id']);
		Tygh::$app['view']->assign('staff_data', $staff_data);
	}

}
