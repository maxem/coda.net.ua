{$obj_prefix = "`$block.block_id`000"}

{if $block.properties.outside_navigation == "Y"}
    <div class="owl-theme ty-owl-controls">
        <div class="owl-controls clickable owl-controls-outside" id="owl_outside_nav_{$block.block_id}">
            <div class="owl-buttons">
                <div id="owl_prev_{$obj_prefix}" class="owl-prev"><i class="ty-icon-left-open-thin"></i></div>
                <div id="owl_next_{$obj_prefix}" class="owl-next"><i class="ty-icon-right-open-thin"></i></div>
            </div>
        </div>
    </div>
{/if}

<div id="scroll_list_{$block.block_id}" class="owl-carousel">
    {foreach from=$items item="staff" name="for_staffs"}
            {include file="common/image.tpl" assign="object_img" class="ty-grayscale" image_width=$block.properties.thumbnail_width image_height=$block.properties.thumbnail_width images=$staff.main_pair no_ids=true lazy_load=true obj_id="scr_`$block.block_id`000`$staff.staff_id`"}
            <div class="ty-center">
                {$object_img nofilter}
                <p>{$staff.f_name} {$staff.l_name}</p>
                <p>{$staff.function}</p>
                <p id="my_get_email_{$staff.staff_id}"><a onclick="$.ceAjax('request', '{$index_script}?dispatch=staff.get_email', {literal}{method: 'POST', data: {staff_id: {/literal} {$staff.staff_id}{literal}}, cache: false, result_ids: 'my_get_email_{/literal}{$staff.staff_id}{literal}'}{/literal});">Get email</a><!-- #my_get_email_{$staff.staff_id} --></p>
            </div>
    {/foreach}
</div>
{include file="common/scroller_init.tpl" items=$items prev_selector="#owl_prev_`$obj_prefix`" next_selector="#owl_next_`$obj_prefix`"}
